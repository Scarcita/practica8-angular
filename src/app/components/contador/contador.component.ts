import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contador',
  templateUrl: './contador.component.html',
  styleUrls: ['./contador.component.css']
})
export class ContadorComponent implements OnInit {

  constructor() { }

  numero: number = 0;
  advertencia:boolean= false
 

  ngOnInit(): void {
  }
  sumar(){
    if (this.numero ===50) {
      this.advertencia=true;
    }else{
      this.numero= this.numero + 1;
      this.advertencia= false;
    }
  }

  resta(){
    if(this.numero===0){
      this.advertencia = true;
    }else{
      this.numero = this.numero -1;
      this.advertencia = false;
    }
  }

}
